//
//  ContentView.swift
//  ReadingTracker
//
//  Created by Diuli Nobre on 26/10/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack {
            Text("Hello, world!")
        }
        .padding()
    }
}

#Preview {
    ContentView()
}
