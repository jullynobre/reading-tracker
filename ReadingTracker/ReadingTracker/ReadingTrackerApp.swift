//
//  ReadingTrackerApp.swift
//  ReadingTracker
//
//  Created by Diuli Nobre on 26/10/23.
//

import SwiftUI

@main
struct ReadingTrackerApp: App {
    @StateObject private var dataController = DataController()
    
    var body: some Scene {
        WindowGroup {
            TabView {
                ContentView()
                    .tabItem {
                        Label("Home", systemImage: "house.fill")
                    }
                    .environment(\.managedObjectContext, dataController.container.viewContext)
                LibraryView()
                    .tabItem {
                        Label("Library", systemImage: "book.fill")
                    }
                
            }
            
        }
    }
}
